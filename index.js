// [Section] While Loop
	// While loop takes in an expression or condition
	// Expressions are any unit of code that cam be evaluated to a value
	// If the condition evaluates to be true, the statements inside the code block will be executed. 
	// A loop will iterate a certain number of times until an expression or condition is met.
	// Once it was met, it will stop.
	// Iteration is the term given to the repitition of statements

	/*
		Syntax:
		While(expression or condition){
			statements;
			iteration;
		}
	*/

/*	let count = 5;
	
	while(count!==0){
	// While the value of count is not equal to zero
		console.log("While: " + count);
		// The current value of count is printed out.
		
		// Decrement
		count--;
		console.log("Value of count after the iteration: ")
		// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
		// Loops occupy a significant amount of memory space in our devices.
		// Make sure that expressions or conditions in loops have their corresponding increment or decremenet operators to stop the loop
		// Forgetting to include this in loops will make our application run an infinite loop
		// After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application, tab, or broswer to avoid this. 
	}*/

// [Section] Do While Loop
	// This works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once. (isang beses mag gguarantee na mag rrun)

	/*
		Syntax:
			do{
				statement;
				iteration;
			}
			While(expression/condition)
	*/

	/*let number = Number(prompt("Give me a number"));
	do{
		console.log("Do while: " + number);

		number++;
	}
	while(number < 10);*/
	// Number() - will change the string into number data type


// [Section] For Loop
/*	
	More flexible than while and do-while 
	It consists of 3 parts
		1. The "initialization" value that will track the progression of the loop
		2. The "expression or conditon" that will be evaluated which will determine whether the loop will run on more time.
		3. The "finalExpression" indicates how to advanced the loop.

	Syntax:
		for(initialization; expression or condition; finalExpression){
			statement/s;
		}
*/

	// We will create a loop that will start from 0 and end at 20
	// Every iteration of the loop, the value of count will be checked if it is equal or less than 20
	// If the vallue of count is less than or equal to 20, the statement inside of the loop will execute
	// The value of cpunt will be incremented by one for each iteration

	/*for(let count = 0; count <= 20; count++){
		console.log("The current value of count is: " + count)
	}*/


// Characters in strings may be counted using the .length property
// Strings are special compare to other data types, that it has access to functions and other pieces of information

	/*let myString = "christopher"
	console.log(myString.length);*/

// We can also access the characters of a string
// Individual characters of string may be accessed using its index number
// The first character in a string correcponds to 0, and then the next is 1, up to the nth or last character.

	/*console.log(myString[0]);
	console.log(myString[1]);
	console.log(myString[2]);
	console.log(myString[3]);*/

	/*for(let i = 0; i < myString.length; i++){
		console.log(myString[i]);
	}*/


// Loop Example for Math Operations
	/*let numA = 15;

	for(let i = 0; i <= 10000; i++){
		let exponential = numA ** i

		console.log(exponential);
	}*/


// Create a string named "myName" with value of "Alex"
	let myName = "Patricia Ysabel";

	// Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is vowel.

	for(let i = 0; i < myName.length; i++){

		if(myName[i] .toLowerCase() === "a" ||
			myName[i] .toLowerCase() === "e" ||
			myName[i] .toLowerCase() === "i" ||
			myName[i] .toLowerCase() === "o" ||
			myName[i] .toLowerCase() === "u"){

			console.log(3);
		}

		else{
			console.log(myName[i]);
		}
	}


// [Section] Continue and Break Statements
/*
	- The "continue" statements allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block. (current loop or iteration, next iteration will continue, skip then proceed to next)
	- The "break" statement is used to terminate the current loop once a match ahs been found. (entire loop)
*/

	for(let count = 0; count <= 20; count++){
		/*if remainder is equal to 0*/
			if (count % 2 === 0) {

				// Tells the code to continue to the next iteration of the loop. This ignores all statements located after the continue statement.
				
				continue;
			}
			
			else if (count >10){
				break;
				// Tells the code to terminate/stop the loop even if the expression or condition of the loop defines that it shoould execute so long as the value of count is less than or equal to 20
				// Number values after 10 will no longer be printed.
			}

			console.log("Continue and Break" + count);
	}


	let name = "alexandro";

	for(let i = 0; i < name.length; i++){
		

		if(name[i] .toLowerCase() === "a"){
			console.log("Continue to the next iteration");
			continue;
		}

		if(name[i] .toLowerCase() === "d"){
			console.log("Console log before the break");
			break;
		}

		console.log(name[i])
	}


// Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop.
/*
	How For Loop works:
	1. The loop will start at 0 for the value of "count"
	2. It will check if "count" is less than the equal to 20.
	3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2)
	4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
    5. If the value of count is not equal to 0, the console will print the value of "count".
    6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
    7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
    8. The value of "count" will be incremented by 1 (e.g. count = 1)
    9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement
*/